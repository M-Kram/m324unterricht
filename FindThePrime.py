# Beispielstruktur
max_num = int(input("Bitte geben Sie die maximale Zahl ein: "))
# Primzahlen finden
def is_prime(max_num):
    if max_num <= 1:
        return False
    if max_num <= 3:
        return True
    if max_num % 2 == 0 or max_num % 3 == 0:
        return False
    i = 5
    while i * i <= max_num:
        if max_num % i == 0 or max_num % (i + 2) == 0:
            return False
        i += 6
    return True

def generate_primes(max_num):
    primes = []
    for num in range(2, max_num + 1):
        if is_prime(num):
            primes.append(num)
    return primes

if max_num < 2:
    print("There are no prime numbers in the given range.")
else:
    prime_numbers = generate_primes(max_num)
    print("Prime numbers up to", max_num, "are:", prime_numbers)
